@extends('Layouts.index')
@section('content')
    <div class="container cont-content ">
<div class="form-group">
    <label for="cat">Category Filter:</label>
    <select name="cat" id="catFilter">
        <option>All</option>
        @foreach($catResult as $cat)
            <option value="{{ $cat->id }}">{{$cat->name}}</option>
        @endforeach
    </select>
</div>
    {{-- Popup for edit admin posts --}}
        <div class="modal fade" id="editPostModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Edit Post</h4>
                    </div>
                    <div id="updatePost"></div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
<table id="newsDtb" class="display" width="100%" cellspacing="0">
    <thead>
    <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Description</th>
        <th>News Content</th>
        <th>News Category</th>
        <th>News Image</th>
        <th>Edit</th>
        <th>Options</th>
    </tr>
    </thead>
    <tbody>
    @foreach($result as $res)
    <tr>
        <td>{{$res->id}}</td>
        <td>{{$res->title}}</td>
        <td>{{$res->desc}}</td>
        <td>{{str_limit(($res->news_content),30)}}</td>
        <td>{{$res->ctg['name']}}</td>
        @if($res->image)
            <td><img src="thumbs/{{$res->image}}" class="dtb-img"></td>
        @else
            <td><img src="thumbs/unknown_picture.jpg" class="dtb-img"></td>
        @endif
        <td><button class="newsEditedBtn" type="submit">Edit</button></td>
        <td><button class="removeBtn" type="submit">Delete</button></td>
    </tr>
    @endforeach
    </tbody>
</table>
    <a href="http://news.loc/">
        <button type="button" class="btn btn-default btn-mg">
            <span class="glyphicon glyphicon-arrow-left"></span> Back Home Page</button>
    </a>
        <a href="{{route('newsCreate')}}"><button type="button" class="btn btn-default">Create New News</button></a>
        <a href="{{route('cat-table')}}"><button type="button" class="btn btn-default">Watch / Create Category</button></a>
</div>

@endsection