<form id="editCatForm" method="post">
    <div class="form-group">
        <label for="editCatName" class="control-label">New Category Name:</label>
        <input type="text" name="name" value="{{$result->name}}" class="form-control" id="editCatName" >
    </div>
    <div class="form-group">
        <label for="editDesc" class="control-label">New Description Name:</label>
        <input type="text" name="desc" value="{{$result->description}}" class="form-control" id="editDesc" >
    </div>

    <button type="button" class="btn btn-success" id="addEditedCat">Accept changes</button>
</form>
