@extends('Layouts.index')
@section('content')

    <div class="container cont-content ">
        {{-- Popup for create new category --}}
    <div class="modal fade" id="createCatModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Create new category</h4>
                </div>
                    <div id="createResult"></div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{-- Popup for Edit category --}}
    <div class="modal fade" id="editCatModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">

        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Edit category</h4>
                </div>
                <div id="updateCat"></div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
{{-- Category Datatable--}}
    <div class="container cont-content ">
        <table id="catDtb" class="display" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>Category Id</th>
                <th>Category Name</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
        @foreach($catResult as $res)
            <tr>
                <td>{{$res->id}}</td>
                <td>{{$res->name}}</td>
                <td><button class="editBtn" type="submit">Edit</button></td>
                <td><button class="removeBtn" type="submit">Delete</button></td>
            </tr>
         @endforeach
            </tbody>
        </table>
    </div>
        <a href="http://news.loc/admin">
            <button type="button" class="btn btn-default btn-mg">
            <span class="glyphicon glyphicon-arrow-left"></span> Back</button>
        </a>
        <button type="button" id="catCreateBtn" class="btn btn-default" data-toggle="modal" data-target="#createCatModal">
            Create New Category</button>
</div>
@endsection