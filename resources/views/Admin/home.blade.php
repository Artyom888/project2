@extends('Layouts.app')
@section('content')
    <div class="container">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li> {{$error }} </li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="post" action="{{route('addNews')}}" enctype="multipart/form-data">
            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" class="form-control" name="title" id="title">
            </div>
            <div class="form-group">
                <label for="desc">Description:</label>
                <input type="text" class="form-control" name="desc" id="desc">
            </div>
            <div class="form-group">
                <label for="cat">Category:</label>
                <select name="cat" id="cat">
                    @foreach($catRes as $cat)
                        <option value="{{$cat->id}}" >{{$cat->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="image">Image upload:</label>
                <input name="image" type="file" id="image">
            </div>
            <div class="form-group">
                <label for="news">News Content:</label>
                <textarea class="form-control" rows="5" name="news" id="news"></textarea>
            </div>
            <button type="submit" class="btn btn-success btn-block">Add News</button>
            {{ csrf_field() }}
        </form>

    </div>
@endsection
