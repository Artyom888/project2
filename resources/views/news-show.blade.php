@extends('Layouts.index')
@section('content')

<div class="container">
    <p> Post created | {{$post->created_at}}</p>
    <h3 class="news-title">{{$post->title}}</h3>
    <p class="news-desc">{{$post->desc}}</p>

    <div>
        @if($post->image)
            <img src="/photos/{{$post->image}}" class="center-block one-post-img" alt="image">
        @else
            <img src="{{asset('photos/unknown_picture.jpg')}}" class="center-block one-post-img" alt="image">
        @endif
    </div>
    <p class="short-content">{{$post->news_content}}</p>


<a href="http://news.loc/">
    <button type="button" class="btn btn-default btn-mg">
        <span class="glyphicon glyphicon-arrow-left"></span> Back Home Page</button>
    </a>
</div>

@endsection