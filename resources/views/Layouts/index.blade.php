<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>News</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{asset('bootstrap-3/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('bootstrap-3/css/bootstrap-theme.min.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css"/>
    <link rel="stylesheet" href="{{asset('css/news.css')}}">

</head>
<body>
<nav class="navbar navbar-default nav-header">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="http://news.loc/">NEWS</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="http://news.loc/">Home</a></li>
         </ul>
    </div>
</nav>


@yield('content')
<script src="\js\jquery-3.2.1.min.js" type="text/javascript" ></script>
<script src="\bootstrap-3\js\bootstrap.min.js"></script>
<script src="\js\news.js" type="text/javascript" ></script>
<script src="\js\category.js" type="text/javascript" ></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

</body>
</html>