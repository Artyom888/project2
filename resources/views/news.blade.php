@extends('Layouts.index')
@section('content')

<div class="container cont-content ">
    <div class="row row-box">

            @foreach($posts as $post)
        <div class="col-xs-8 col-sm-3 col-md-3 col-lg-2" >
            <div>
                @if($post->image)
                    <img src="photos/{{ $post->image }}" class="post-img center-block" alt="post image">
                @else
                    <img src="photos/unknown_picture.jpg" class="post-img center-block"  alt="post image">
                @endif
            </div>
            <hr>
        <div class="post-field">
            <h3 class="news-title">{{ str_limit(($post->title),15) }}</h3>
            <p class="news-desc">{{str_limit(($post->desc),17)}}</p>
        </div>
        <div class="read-more">
            <a href="{{route('showNews',['id'=>$post->id]) }}" >
                <button class="btn btn-info btn-sm">Read more</button>
            </a>
        </div>
            <div>
                <button type="button" id="post_{{ $post->id }}" class="btn btn-primary btn-xs likeBtn">Like</button>
                <p class="badge likeCount">{{$post->likes}}</p>
            </div>

        </div>

           @endforeach
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <ul class="pagination pagination-lg">
             {{$posts}}
        </ul>
    </div>
</div>


@endsection
