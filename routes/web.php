<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'NewsController@getAll');
Route::get('news/{id}','NewsController@show')->name('showNews');
Route::post('/post/likes','NewsController@getLikes');

Auth::routes();

Route::group (['prefix'=>'admin','middleware'=>['web','auth']],function(){
    Route::get('/news/create','NewsController@getAllCategories')->name('newsCreate');
    Route::post('/news/create',['uses' => 'NewsController@addNews','as' => 'addNews']);
    Route::post('/news/delete','NewsController@destroy');
    Route::get('/','Admin\AdminController@getAll');
    Route::post('/post/edit','NewsController@view');
    Route::post('/edit/add','NewsController@addChanges');
    //Category

     Route::get('category','Admin\CategoryController@showTable')->name('cat-table');
     Route::post('category/delete','Admin\CategoryController@destroy');
     Route::post('category/edit','Admin\CategoryController@view');
     Route::post('category/edit/add','Admin\CategoryController@addChanges')->name('add-changes');
     Route::get('category/create','Admin\CategoryController@create');
     Route::post('category/create','Admin\CategoryController@addCategory')->name('add-category');

});


