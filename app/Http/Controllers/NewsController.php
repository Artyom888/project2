<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Post;
use App\Category;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
class NewsController extends Controller
{
    //Posts likes
    public function getLikes(Request $request){
        $response = ['success' => false];
        $quantity = $request->input('quantity');
        $quantity = ++$quantity;
        $id = $request->input('id');
        $rand = 'user'. rand(1, 5000);
        $rand = md5($rand);
        if($quantity && $id) {
            $query = Post::where('id', $id)->update([
                'likes' => $quantity
               ]);
            if($query){
                $response = ['success' => true];
                $response['message'] = 'The post like updated';
                $response['quantity'] = $quantity;

            }
        }else{
            $response['message'] = 'The post like not updated:error';
        }
        return response($response);
    }

    // Show All posts
  public function getAll(Request $request){
      $query = Post::select('id','title','desc','image','likes');
      $posts = $query->paginate(10);
      return view('news')->with('posts',$posts);
  }
  // Show one post
  public function show($id){
      $result = Post::select('title','desc','news_content','image','created_at')->where('id',$id)->first();

      return view('news-show')->with('post',$result);
  }
  // Add post
    public function addNews(Request $request){
        $messages = [
            'required'=> 'Fill all fields!',
        ];
        $rules = [
            'title' => 'required|max:100' ,
            'desc' => 'required|max:100' ,
            'news' => 'required',
            'cat' => 'required'
        ];
        if($request->hasFile('image')){
            $rules['image'] = 'image|mimes:jpeg,jpg,png,gif';

        }
        $this->validate($request,$rules,$messages);
        $title = $request->input('title');
        $desc = $request->input('desc');
        $news = $request->input('news');
        $category = $request->input('cat');
        $params = [
            'title' => $title,
            'desc' => $desc,
            'news_content' => $news,
            'category_id' => $category
        ];
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $currentTimestamp = Carbon::now()->timestamp;
            $imgName = rand().$currentTimestamp;
            $extension = $file->getClientOriginalExtension();
            $thumbImg = Image::make($file->getRealPath())->resize(70, 50);
            $thumbImg->save('thumbs'.'/'. $imgName.'.'.$extension,100);
            $file->move('photos', $imgName.'.'.$extension);
            $params['image'] = $imgName.'.'.$extension ;
        }
        $query = Post::create($params);
        if($query){
            return redirect()->back();
        }
    }
    //Admin post edit form view
    public function view(Request $request)
    {
        $id = $request->input('id');
        if ($id) {
            $result = Post::select('title', 'desc','news_content')->where('id', $id)->first();
            return response(view('Admin/postsEditForm',array('result'=>$result)),200);
        } else {
            return response('An error has occurred');
        }
    }
    // add post edit
    public function addChanges(Request $request){
        $response = ['success' => false];
        $title = $request->input('title');
        $desc = $request->input('desc');
        $content = $request->input('content');
        $id = $request->input('id');
        if($title && $desc && $content && $id){
            $result = Post::where('id',$id)->update([
                'title' => $title,
                'desc' => $desc,
                'news_content' => $content
            ]);
            if($result){
                $response['success'] = true;
                $response['message'] = 'The post updated';
            }
        }else{
            $response['message'] = 'The post has not updated ! Repeat again';
        }
        return response()->json($response);

    }
  //Delete post
    public function destroy(Request $request)
    {
        $response = ['success' => false];
        $id = $request->input('id');
        if ($id) {
            $imgName = Post::find($id);
            if($imgName->image != NULL){
            $photosPath = public_path().'/photos/'.$imgName->image;
            $thumbsPath = public_path().'/thumbs/'.$imgName->image;
            if(file_exists($photosPath) && file_exists($thumbsPath)) {
                unlink($photosPath);
                unlink($thumbsPath);
                }
            }
            $result = Post::destroy($id);
            if($result) {
                $response['success'] = true ;
            }
        }else{
            $response['message'] = 'wrong id';
        }
        return response()->json($response);
    }
    // Get Category for create post form
    public function getAllCategories()
    {
        $catResult = Category::select('id','name')->get();
        return view('Admin\home')->withCatRes($catResult);
    }

}
