<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Post;
use Illuminate\Support\Facades\Response;
use phpDocumentor\Reflection\Types\Null_;

class CategoryController extends Controller
{
    public  function showTable(){
        $catResult = Category::select('id','name')->get();
        return view('Admin/category')->with('catResult',$catResult) ;
   }
   public function destroy(Request $request){
       $response = ['success' => false];
       $id = $request->input('id');
       if ($id) {
           $ctg = Category::find($id);
           $catPost = $ctg->posts;
           foreach ($catPost as $post) {
               $postCtg = $post->image;
           }
        $posts = Post::select('image')->where('category_id',$id)->first();
        if(isset($postCtg) != NULL){
            $photosPath = public_path().'/photos/'.$posts->image;
            $thumbsPath = public_path().'/thumbs/'.$posts->image;
            if(file_exists($photosPath) && file_exists($thumbsPath)) {
                unlink($photosPath);
                unlink($thumbsPath);
            }
        }
       $result = Category::destroy($id);
           if($result) {
               $response['success'] = true ;
           }
       }else{
           $response['message'] = 'wrong id';
       }
       return response()->json($response);
   }

    //Add Category
    public function addCategory(Request $request){
        $response = ['success' => false];
        $name = $request->input('name');
        $desc = $request->input('desc');
        if($name && $desc){
            $result = Category::create([
                'name' => $name,
                'description' => $desc
            ]);
            if($result){
                $response['success'] = true;
                $response['message'] = 'The category created';
            }
        }else{
            $response['message'] = 'The category has not created ! Repeat again';
        }
        return response()->json($response);
    }
    // View Create Form
    public function create(){
        return response()->view('Admin/create-modal-form');
    }
    //Category edit form view
    public function view(Request $request)
    {
        $id = $request->input('id');
        if ($id) {
            $result = Category::select('name', 'description')->where('id', $id)->first();
            return response(view('Admin/catEditForm',array('result'=>$result)),200);
        } else {
            return response('An error has occurred');
        }
    }
    public function addChanges(Request $request){
        $response = ['success' => false];
        $name = $request->input('name');
        $desc = $request->input('desc');
        $id = $request->input('id');
        if($name && $desc && $id){
            $result = Category::where('id',$id)->update([
                'name' => $name,
                'description' => $desc
                ]);
            if($result){
                $response['success'] = true;
                $response['message'] = 'The category updated';
            }
        }else{
            $response['message'] = 'The category has not updated ! Repeat again';
        }
        return response()->json($response);

    }
}
