<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Category;
class AdminController extends Controller
{
   //
    public function getAll(Request $request){
        $result = Post::with ('ctg')->get();

        $catResult = Category::select('id','name')->get();
        return view('Admin/show')->with('result',$result)->with('catResult',$catResult);
    }


}
