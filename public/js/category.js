var catTable;
$(function () {
    //NEws Category Datatable
    catTable = $("#catDtb").DataTable({});
    //News Category Datatable row remove
    $('#catDtb').on('click', '.removeBtn', function () {
        var row = $(this).parents('tr')[0];
        var rowId = $(this).parents('tr')[0].childNodes[1].innerHTML;
        var data = {id:rowId};
        if (rowId && confirm('Admin: Delete this category ?')) {
            $.ajax({
                type: "POST",
                url: "http://news.loc/admin/category/delete",
                data:data,
                success: function (data) {
                    if (data.success) {
                        row.remove();
                    }
                }
            });
        }
    });

    // News : Create Category
    $('#catCreateBtn').click(function () {
        $.ajax({
            type: "GET",
            url: "http://news.loc/admin/category/create",
            success:function (data) {
                $('#createCatModal .modal-body').html(data);
            }
        });
    });
// Add new category
    $('body').on('click', '.addCatBtn', function () {
        var createCatName = $('#createName').val();
        var createCatDesc = $('#createDesc').val();
        var data = {name:createCatName,desc:createCatDesc};
        $.ajax({
            type: "POST",
            url: "http://news.loc/admin/category/create",
            data:data,
            success:function (data) {
                $('#createResult').text(data.message);
            }
        });
    });

//News Category Datatable Edit Category
    var editRowId;
    $('#catDtb').on('click', '.editBtn', function () {
        editRowId = $(this).parents('tr')[0].childNodes[1].innerHTML;
        $.ajax({
            type: "POST",
            url: "http://news.loc/admin/category/edit",
            data:{id:editRowId},
            success:function (data) {
                $('#editCatModal .modal-body').html(data);
            }
        });
        $("#editCatModal").modal('show');

    });
//Add Edited Category
    $('body').on('click', '#addEditedCat', function (e) {
        var editCatName = $('#editCatName').val();
        var editDesc = $('#editDesc').val();
        var data = {name:editCatName,desc:editDesc,id:editRowId};
        $.ajax({
            type: "POST",
            url: "http://news.loc/admin/category/edit/add",
            data:data,
            success:function (data) {
                $('#updateCat').text(data.message);
            }
        });
    });
});