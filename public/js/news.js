var table;
$(function () {
    table = $("#newsDtb").DataTable({});
// News Datatable filtering
    $('#catFilter').change(function () {
        var optText = $("#catFilter option:selected").text();
        table.columns(4).search(optText).draw();
        if (optText === 'All') {
            table.columns(4).search('').draw();
        }
    });

//News Datatable row remove
    $('#newsDtb').on('click', '.removeBtn', function () {
        var row = $(this).parents('tr')[0];
        var rowId = row.childNodes[1].innerHTML;
        var data = {id: rowId};
        if (rowId && confirm('Admin: Delete this news ?')) {
            $.ajax({
                type: "POST",
                url: "http://news.loc/admin/news/delete",
                data: data,
                success: function (data) {
                    if (data.success) {
                        row.remove();
                        console.log(data);
                    }
                }
            });
        }
    });

// News datatable eidt posts
    var editRowId;
    $('#newsDtb').on('click', '.newsEditedBtn', function () {
        editRowId = $(this).parents('tr')[0].childNodes[1].innerHTML;
        var row = $(this).parents('tr')[0];
        $.ajax({
            type: "POST",
            url: "http://news.loc/admin/post/edit",
            data: {id: editRowId},
            success: function (data) {
                $('#editPostModal .modal-body').html(data);
            }
        });
        $("#editPostModal").modal('show');
    });

// Add update post
    $('body').on('click', '#addEditedPost', function (e) {
        var title = $('#editPostTitle').val();
        var desc = $('#editPostDesc').val();
        var content = $('#editPostContent').val();
        var data = {title: title, desc: desc, content: content, id: editRowId};
        $.ajax({
            type: "POST",
            url: "http://news.loc/admin/edit/add",
            data: data,
            success: function (data) {
                $('#updatePost').text(data.message);
            }
        });
    });
//post  likes
    $('body').on('click', '.likeBtn', function () {
        var likeBtn = $(this);
        var idStr = $(this).attr('id');
        var postId = idStr.split('_');
        postId = postId[1];
        var badge = $(this).next();
        var quantity = 1 * ( badge.html());
        var data = {quantity: quantity, id: postId};
        var value = [postId];
        $.ajax({
            type: "POST",
            url: "http://news.loc/post/likes",
            data: data,
            success: function (data) {
                if (data.success) {
                    badge.html(data.quantity);
                    if (!(getCookie('postsLike'))) {
                        document.cookie = 'postsLike=' + JSON.stringify(value);
                    } else {
                        var elem = JSON.parse(getCookie('postsLike'));
                        elem.push(postId);
                        document.cookie = 'postsLike=' + JSON.stringify(elem);
                    }
                    likeBtn.attr('disabled', true).css("cursor", "default");
                }
            }
        });
    });
    if (getCookie('postsLike')) {
        var elem = JSON.parse(getCookie('postsLike'));
        for (var i = 0; i < elem.length; i++) {
            $('#post_' + elem[i]).attr('disabled', true).css('cursor', 'default');
        }
    }
});

//get cookie
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}


